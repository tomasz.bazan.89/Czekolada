using System;
using System.Collections.Generic;
using System.Text;

namespace czekolada
{
    class Chocolate : ChocoProducts, Interface1
    {
        protected string kind;

        public override string getName()
        {
            return name;
        }
        public override void setName(string name)
        {
            this.name = name;
        }
        public override string getProducer()
        {
            return producer;
        }
        public override void setProducer(string producent)
        {
            this.producer = producer;
        }
        public override int getWeight()
        {
            return weight;
        }
        public override void setWeight(int weight)
        {
            this.weight = weight;
        }
        public override double getPrice()
        {
            return price;
        }
        public override void setPrice(double price)
        {
            this.price = price;
        }
        public string getKind()
        {
            return kind;
        }
        public void setKind(string kind)
        {
            this.kind = kind;
        }
        public override void print()
        {
            Console.WriteLine("Name: " + name);
            Console.WriteLine("Producent: " + producer);
            Console.WriteLine("Weight: " + weight);
            Console.WriteLine("Price: " + price);
            Console.WriteLine("Kind" + kind);
        }
    }
}
