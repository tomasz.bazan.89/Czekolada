using System;
using System.Collections.Generic;
using System.Text;

namespace czekolada
{
    class CandyBar : ChocoProducts, Interface1
    {
        protected string description;

        public override string getName()
        {
            return name;
        }
        public override void setName(string name)
        {
            this.name = name;
        }
        public override string getProducent()
        {
            return producent;
        }
        public override void setProducent(string producent)
        {
            this.producent = producent;
        }
        public override int getWeight()
        {
            return weight;
        }
        public override void setWeight(int weight)
        {
            this.weight = weight;
        }
        public override double getPrice()
        {
            return price;
        }
        public override void setPrice(double price)
        {
            this.price = price;
        }
        public string getDescription()
        {
            return description;
        }
        public void setDescription(string description)
        {
            this.description = description;
        }
        public override void print()
        {
            Console.WriteLine("Name: " + name);
            Console.WriteLine("Producent: " + producent);
            Console.WriteLine("Weight: " + weight);
            Console.WriteLine("Price: " + price);
            Console.WriteLine("Description" + description);
        }
    }
}
