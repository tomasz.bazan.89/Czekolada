using System;
using System.Collections.Generic;
using System.Text;

namespace czekolada
{
    class Other:ChocoProducts, Interface1
    {
        protected int piecesInPack;

        public override string getName()
        {
            return name;
        }
        public override void setName(string name)
        {
            this.name = name;
        }
        public override string getProducer()
        {
            return producer;
        }
        public override void setProducer(string producent)
        {
            this.producer = producer;
        }
        public override int getWeight()
        {
            return weight;
        }
        public override void setWeight(int weight)
        {
            this.weight = weight;
        }
        public override double getPrice()
        {
            return price;
        }
        public override void setPrice(double price)
        {
            this.price = price;
        }
        public int getPiecesInPack()
        {
            return piecesInPack;
        }
        public void getPiecesInPack(int getPiecesInPack)
        {
            this.piecesInPack = getPiecesInPack;
        }
        public override void print()
        {
            Console.WriteLine("Name: " + name);
            Console.WriteLine("Producent: " + producer);
            Console.WriteLine("Weight: " + weight);
            Console.WriteLine("Price: " + price);
            Console.WriteLine("Pieces in Pack: " + piecesInPack);
        }
    }
}
