using System;
using System.Collections.Generic;
using System.Text;

namespace czekolada
{
     class CandyBar : ChocoProducts, ChocoShop
    {
        private string description;

        public CandyBar()
            :base()
        {
            description = "";
        }

         public CandyBar(string name, string producer, int weight, double price,string description)
            :base(name, producer, weight, price)
        {
            this.description=description;
        }

        public string getDescription()
        {
            return description;
        }
        public void setDescription(string description)
        {
            this.description = description;
        }
    }
}
