using System;
using System.Collections.Generic;
using System.Text;

namespace czekolada
{
    interface Interface1
    {
        string getName();
        void setName(string name);
        string getProducer();
        void setProducer(string producent);
        int getWeight();
        void setWeight(int weight);
        double getPrice();
        void setPrice(double price);
        void print();
    }
}
