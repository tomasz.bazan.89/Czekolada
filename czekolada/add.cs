﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.IO;

namespace czekolada
{

    public partial class Add : Form
    {
        public delegate void AddChoco(ChocoProducts product);
        public event AddChoco AddItemCallback;

        public Add()
        {
            InitializeComponent();
            comboBox1.Text = "Chocolate";
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            switch (comboBox1.SelectedIndex)
            {
                case 0:
                    {
                        textBoxKind.Enabled = true;
                        textBoxPiecesInPack.Enabled = false;
                        textBoxDescription.Enabled = false;
                        break;
                    }
                case 1:
                    {
                        textBoxKind.Enabled = false;
                        textBoxPiecesInPack.Enabled = false;
                        textBoxDescription.Enabled = true;
                        break;
                    }
                case 2:
                    {
                        textBoxKind.Enabled = false;
                        textBoxPiecesInPack.Enabled = true;
                        textBoxDescription.Enabled = false;
                        break;
                    }
            }

        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (weryfikacjaDanych() == DialogResult.OK)
            {
                switch (comboBox1.SelectedIndex)
                {
                    case 0:
                        {

                            Chocolate choco = new Chocolate();
                            choco.setName(textBoxName.Text);
                            choco.setProducer(textBoxProducer.Text);
                            choco.setPrice(Convert.ToDouble(textBoxPrice.Text));
                            choco.setWeight(Convert.ToInt32(textBoxWeight.Text));
                            choco.setKind(textBoxKind.Text);
                            AddItemCallback(choco);
                            this.Close();

                            break;
                        }
                    case 1:
                        {
                            CandyBar bar = new CandyBar();
                            bar.setName(textBoxName.Text);
                            bar.setProducer(textBoxProducer.Text);
                            bar.setPrice(Convert.ToDouble(textBoxPrice.Text));
                            bar.setWeight(Convert.ToInt32(textBoxWeight.Text));
                            bar.setDescription(textBoxDescription.Text);
                            AddItemCallback(bar);
                            this.Close();

                            break;
                        }
                    case 2:
                        {

                            Other ot = new Other();
                            ot.setName(textBoxName.Text);
                            ot.setProducer(textBoxProducer.Text);
                            ot.setPrice(Convert.ToDouble(textBoxPrice.Text));
                            ot.setWeight(Convert.ToInt32(textBoxWeight.Text));
                            ot.setPiecesInPack(Convert.ToInt32(textBoxPiecesInPack.Text));
                            AddItemCallback(ot);
                            this.Close();

                            break;
                        }

                }
            }
        }
            

        

        private void textBoxProducer_TextChanged(object sender, EventArgs e)
        {
            if (textBoxProducer.Text != "")
            {
                textBoxPrice.Enabled = true;
            }
        }

        private void textBoxPrice_TextChanged(object sender, EventArgs e)
        {
            if (textBoxProducer.Text != "")
            {
                textBoxWeight.Enabled = true;
            }
        }

        private void textBoxName_TextChanged(object sender, EventArgs e)
        {
            if (textBoxName.Text != "")
            {
                textBoxProducer.Enabled = true;
            }
            else
            {
                textBoxProducer.Enabled = false;
            }
        }

        private DialogResult weryfikacjaDanych()
        {
            int licznikBledow = 0;
            if (textBoxName.Text == "")
            {
                licznikBledow++;
                MessageBox.Show("Wpisz nazwę");
            }
            if (textBoxProducer.Text == "")
            {
                licznikBledow++;
                MessageBox.Show("Wpisz nazwę producenta");
            }
            if (textBoxPrice.Text == "")
            {
                licznikBledow++;
                MessageBox.Show("Wpisz cenę");
            }
            double price;
            if (!(Double.TryParse(textBoxPrice.Text, out price)))
            {
                licznikBledow++;
                MessageBox.Show("Wpisz poprawnie cenę");
            }
            if (price <= 0)
            {
                licznikBledow++;
                MessageBox.Show("Wartość ceny jest liczbą dodatnią");
            }
            if (textBoxWeight.Text == "")
            {
                licznikBledow++;
                MessageBox.Show("Wpisz wagę");
            }
            int weight;
            if (!(Int32.TryParse(textBoxWeight.Text, out weight)))
            {
                licznikBledow++;
                MessageBox.Show("Wpisz poprawnie wagę");
            }
            if (weight <= 0)
            {
                licznikBledow++;
                MessageBox.Show("Wartość wagi jest liczbą dodatnią");
            }
            switch (comboBox1.SelectedIndex)
            {
                case 1:
                    {
                        if (textBoxKind.Text == "")
                        {
                            licznikBledow++;
                            MessageBox.Show("Wpisz rodzaj");
                        }
                        break;
                    }
                case 2:
                    {
                        if (textBoxDescription.Text == "")
                        {
                            licznikBledow++;
                            MessageBox.Show("Uzupełnij opis");
                        }
                        break;
                    }
                case 3:
                    {
                        if (textBoxPiecesInPack.Text == "")
                        {
                            licznikBledow++;
                            MessageBox.Show("Uzupełnij ilość sztuk w paczce");
                        }
                        int piecesInPack;
                        if (!(Int32.TryParse(textBoxPiecesInPack.Text, out piecesInPack)))
                        {
                            licznikBledow++;
                            MessageBox.Show("Wpisz poprawnie ilość sztuk w paczce");
                        }
                        break;
                    }

            }
            if (licznikBledow == 0)
            {
                return DialogResult.OK;
            }
            else
            {
                return DialogResult.Cancel;
            }

        }
    }
}
