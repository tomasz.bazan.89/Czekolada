using System;
using System.Collections.Generic;
using System.Text;

namespace czekolada
{
    class Other:ChocoProducts, ChocoShop
    {
        protected int piecesInPack;

        public Other()
            : base()
        {
            piecesInPack = -1;
        }

        public Other(string name, string producer, int weight, double price,int piecesInPack)
            :base(name, producer, weight, price)
        {
            this.piecesInPack=piecesInPack;
        }
        
        public int getPiecesInPack()
        {
            return piecesInPack;
        }
        public void setPiecesInPack(int piecesInPack)
        {
            this.piecesInPack = piecesInPack;
        }
        public void getPiecesInPack(int getPiecesInPack)
        {
            this.piecesInPack = getPiecesInPack;
        }
    }
}
