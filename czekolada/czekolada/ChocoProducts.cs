using System;
using System.Collections.Generic;
using System.Text;

namespace czekolada
{
    abstract class ChocoProducts:Interface1
    {
        protected string name;
        protected string producer;
        protected int weight;
        protected double price;
        
        abstract public string getName();
        abstract public void setName(string name);
        abstract public string getProducer();
        abstract public void setProducer(string producent);
        abstract public int getWeight();
        abstract public void setWeight(int weight);
        abstract public double getPrice();
        abstract public void setPrice(double price);
        abstract public void print();
    }
}
